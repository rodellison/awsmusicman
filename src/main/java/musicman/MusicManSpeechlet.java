/**
    Copyright 2014-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.

    Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

        http://aws.amazon.com/apache2.0/

    or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package musicman;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.*;
import com.amazon.speech.ui.*;


import com.amazonaws.ClientConfiguration;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

//import com.jayway.jsonpath.Configuration;
//import com.jayway.jsonpath.JsonPath;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.net.URLEncoder;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This MusicManSpeechlet class is the Main worker invoked from the RequestStreamHandler. The Music Man Speechlet (Alexa Skill) is passed
 * a variety of user intents. The intents and the session data allow this code to call a third party API called Songkick - passing Artist
 * or Venue text. The text, processed by Songkick results in the return of Calendar data for a Musical Artist or Venue respectively.
 * That (JSON) data is processed, parsed and returned through Alexa where it is read/spoken out to the consumer.
 * handler for the AWS Lambda skill. It will validate the request is coming from the Alexa service

 */
public class MusicManSpeechlet implements Speechlet {

    /**
     * Array of month names.
     */
    private static final String[] MONTH_NAMES = {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
    };

   /**
     * Constant defining number of events to be read at one time.
     */
    private static final int PAGINATION_SIZE = 3;

    /**
     * Constant defining session attribute key for the event index.
     */
    private static final String SESSION_ARTISTVENUE = "artistvenue";

    /**
     * Constant defining session attribute key for the event index.
     */
    private static final String SESSION_INDEX = "index";

    /**
     * Constant defining session attribute key for the event text key for date of events.
     */
    private static final String SESSION_TEXT = "text";

    /**
     * Constant defining session attribute key for the intent slot key for the date of events.
     */
    private static final String SLOT_DAY = "day";

    /**
     * Slots that will come in one of the intents
     */
    private static final String ARTIST_SLOT = "artist";
    private static final String VENUE_SLOT = "venue";

    /**
     * Image object that will hold the URLs to the images provided for user's Alexa app cards
     */
    private static Image myImage;

    /**
     * Log4J object
     */
    private static final Logger log = LoggerFactory.getLogger(MusicManSpeechlet.class);

    private  int currentIndex = 0;
    private  String strSongKickAPIID = "";
    private  String strTheArtist = "";
    private  String strTheVenue = "";
    private  String strArtistVenue = "";


    @Override
    public void onSessionStarted(final SessionStartedRequest request, final Session session)
            throws SpeechletException {

        log.warn("onSessionStarted requestId={" + request.getRequestId() + "}, sessionId={" + session.getSessionId() + "}");
        // any initialization logic goes here
        Properties prop = new Properties();
        myImage = new Image();

        try {

            // load a properties file
            myImage.setSmallImageUrl("https://s3.amazonaws.com/content.musicman/images/Songkick108.png");
            myImage.setLargeImageUrl("https://s3.amazonaws.com/content.musicman/images/Songkick512.png");
            prop.load(getClass().getResourceAsStream("../config.properties"));
            if (prop.size() > 0) {
                strSongKickAPIID = prop.getProperty("apikey");
            } else {
                log.error("AWS Skill Music Man cannot find config.properties file.");
            }
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    @Override
    public SpeechletResponse onLaunch(final LaunchRequest request, final Session session)
            throws SpeechletException {
        log.warn("onLaunch requestId={" + request.getRequestId() + "}, sessionId={" + session.getSessionId() + "}");
        return getWelcomeResponse();
    }

    @Override
    public SpeechletResponse onIntent(final IntentRequest request, final Session session)
            throws SpeechletException {

        log.warn("onIntent requestId={" + request.getRequestId() + "}, sessionId={" + session.getSessionId() + "}");

        Intent intent = request.getIntent();
        String intentName = null;
        if (intent != null) {
            intentName = intent.getName();
            log.info("onIntent: Processing " + intentName);
        } else {
            log.info("onIntent: Did not hear intent");
        }

        if ("MusicManArtistIntent".equals(intentName) || "MusicManVenueIntent".equals(intentName)) {
            log.warn("calling getNewEventsResponse");
            return getNewEventsResponse(intent, session, intentName);
        } else if ("GetNextEventIntent".equals(intentName)) {
            log.warn("calling handleNextEventRequest");
            return handleNextEventRequest(session);
        } else if ("HelpIntent".equals(intentName)) {
            log.warn("calling getHelpResponse");
            return getHelpResponse();
        } else if ("AMAZON.HelpIntent".equals(intentName)) {
            log.warn("calling getHelpResponse");
            return getHelpResponse();
        } else if ("HelpEvents".equals(intentName)) {
            log.warn("calling getHelpResponse");
            return getHelpResponse();
         } else if ("IncompleteIntents".equals(intentName)) {
            log.warn("calling returnFailSpeech");
            return returnFailSpeech("The Music Man didn't understand your last question. Can you please ask it again, or say Help, if you need assistance. ");
        } else if ("AMAZON.StopIntent".equals(intentName)) {
            log.warn("calling newTellResponse - processing a Stop Intent");
            session.removeAttribute(SESSION_INDEX);
            session.removeAttribute(SESSION_TEXT);
            session.removeAttribute(SESSION_ARTISTVENUE);
            return newTellResponse("Goodbye", false);
        } else if ("StopEvents".equals(intentName)) {
            log.warn("calling newTellResponse - processing a Stop Intent");
            session.removeAttribute(SESSION_INDEX);
            session.removeAttribute(SESSION_TEXT);
            session.removeAttribute(SESSION_ARTISTVENUE);
             return newTellResponse("Goodbye", false );
        } else if ("AMAZON.CancelIntent".equals(intentName)) {
            log.warn("calling newTellResponse - processing a Cancel Intent");
            session.removeAttribute(SESSION_INDEX);
            session.removeAttribute(SESSION_TEXT);
            session.removeAttribute(SESSION_ARTISTVENUE);
             return newTellResponse("Goodbye", false );
        } else {
            log.warn("calling getHelpResponse");
            return getHelpResponse();
        }
    }

    @Override
    public void onSessionEnded(final SessionEndedRequest request, final Session session)
            throws SpeechletException {
        log.warn("onSessionEnded requestId={" + request.getRequestId() + "}, sessionId={" + session.getSessionId() + "}");
    }

    /**
     * Prepares the speech to reply to the user. handleNextEventRequest is invoked when the user has
     * responded affirmative to the last response indicating more data was available.
     *
     * @param session
     *            object containing session attributes with events list and index
     * @return SpeechletResponse object with voice/card response to return to the user
     */
    protected SpeechletResponse handleNextEventRequest(Session session) {
        ArrayList<String> events;
        try {
            events = (ArrayList<String>) session.getAttribute(SESSION_TEXT);
            currentIndex = (Integer) session.getAttribute(SESSION_INDEX);
            strArtistVenue = (String) session.getAttribute(SESSION_ARTISTVENUE);
        } catch (Exception ex){
           log.error("Error in handleNextEventsRequest: " + ex.getMessage()) ;
            return returnFailSpeech("Sorry, I couldn't understand the Artist or Location you were looking for. Please try your question again. ");
        }

        String cardTitle = "More events for " + strArtistVenue;
        String speechOutput = "";
        String repromptText = "";
        StringBuilder speechOutputBuilder = new StringBuilder();
        StringBuilder cardOutputBuilder = new StringBuilder();

        if (null == events || (currentIndex >= events.size())) {
            speechOutput = "I don't see any more events. Thanks for using The Music Man.";
            SsmlOutputSpeech outputSpeech = new SsmlOutputSpeech();
            outputSpeech.setSsml("<speak>" + speechOutput + "</speak>");
            return SpeechletResponse.newTellResponse(outputSpeech);
         } else {
             for (int i = 0; i < PAGINATION_SIZE && currentIndex < events.size(); i++) {
                try {
                    if (null != events.get(currentIndex)) {
                        speechOutputBuilder.append("<s>");
                        speechOutputBuilder.append(events.get(currentIndex));
                        speechOutputBuilder.append("</s> ");
                        cardOutputBuilder.append(events.get(currentIndex));
                        cardOutputBuilder.append("\n");
                        currentIndex++;
                    }
                } catch (Exception ex) {
                    //May have been less than 3 entries, so an out of bounds exception may have been thrown.
                    //lets just get out
                }
            }
            if (events.size() > currentIndex) {
                speechOutputBuilder.append(" Would you like to hear more?");
                cardOutputBuilder.append(" Would you like to hear more?");
                repromptText = " Would you like to hear more?";
                // If there are more than 3 events, set the count to the currentIndex and add the events
                // to the session attributes
                session.setAttribute(SESSION_INDEX, currentIndex);
                session.setAttribute(SESSION_TEXT, events);
                session.setAttribute(SESSION_ARTISTVENUE, strArtistVenue);

            } else {
                repromptText = "";
            }

            cardOutputBuilder.append("\r\nEvent data provided by Songkick - visit www.songkick.com");

            StandardCard card2 = new StandardCard();
            card2.setTitle(cardTitle);
            card2.setText(cardOutputBuilder.toString());
            card2.setImage(myImage);

            speechOutput = speechOutputBuilder.toString();
            //final cleanups to remove troublesome SSML chars
            speechOutput = speechOutput.replace("(", "");
            speechOutput = speechOutput.replace(")", "");
            speechOutput = speechOutput.replace("&", " and ");

            log.warn("Providing response to user: " + speechOutput);

            SpeechletResponse response;
            //From above, repromptText was set if more data was available, and we need to ask the user
            //if they want to hear more.. If so, use the newAskResponse, if no pagination, then use newTellResponse
            if (repromptText != "") {
                response  = newAskResponse("<speak>" + speechOutput + "</speak>", true, repromptText, false);

            } else {
                response = newTellResponse("<speak>" + speechOutput + "</speak>", true);
                response.setShouldEndSession(true);
            }
            response.setCard(card2);
            return response;
        }
    }

    /**
     * Prepares the speech to reply to the user. returnFailSpeech is invoked when the user has
     * responded affirmative to the last response indicating more data was available.
     *
     * @param message
     *            String object containing message to be sent to user. This function is invoked when the
     *            service encounted an issue in understanding the users intent.
     *
     * @return SpeechletResponse object with voice/card response to return to the user
     */
    protected SpeechletResponse returnFailSpeech(String message) {
        String speechOutput, repromptText,cardPrefixContent, cardTitle;

        String strCommonMessage = message;

        speechOutput = strCommonMessage;
        cardTitle = "The Music Man couldn't understand the artist or location in your question.";
        cardPrefixContent = "You may have success trying one of these shorter queries:\n";
        cardPrefixContent += "Alexa, Ask The Music Man where {artist} is playing. \n";
        cardPrefixContent += "OR \n";
        cardPrefixContent += "Alexa, Ask The Music Man who's coming to {venue} \n";

        SpeechletResponse response;
        repromptText = message;
        response = newAskResponse("<speak>" + speechOutput + "</speak>", true, repromptText, false);
        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle(cardTitle);
        card.setContent(cardPrefixContent);
        response.setCard(card);
        return response;

    }

    /**
     * getNewEventsResponse creates and returns a {@code SpeechletResponse} with providing details about where a requested artist is playing next.
     * By default, the service will respond with up to 3 items. If there were 3 or less items then a Tell response is provided, if there are more then
     * the data is stored in session variables and an Ask response is provided to the user.
     *
     * @param intent contains the intent data which will contain the 'slots' values as intepreted by Alexa.
     * @param session   contains the session information that will be used for storing attributes that may be needed in the next request..i.e.
     *                  if there is more data that what will be provided in this return, the session attributes will store it for later.
     * @param intentName contains the specific intent name that was invoked as a result of Alexa hearing the user. This parm will
     *                   be used to help determine which slot values should be present.
     * @return SpeechletResponse spoken and visual response for the given intent
     */
    protected SpeechletResponse getNewEventsResponse(final Intent intent, final Session session, final String intentName) {

        // Get the slots from the intent.
        Map<String, Slot> slots = intent.getSlots();
        Slot myArtistSlot, myVenueSlot;
        ArrayList<String> events;
        String speechOutput, repromptText,speechPrefixContent,cardPrefixContent, cardTitle;
        // Get the Artist or Venue slot from the list of slots.
        //If it cant be found, we need to ask the user to reform his question.
        //e.g. artist "walk the moon", when asked in the form of ..Alexa, Ask Music Man where is walk the moon
        //returns NO slot value
        if  ("MusicManArtistIntent".equals(intentName))  {
            try {
                myArtistSlot = slots.get(ARTIST_SLOT);
                if (null == myArtistSlot) {
                    return returnFailSpeech("I couldn't understand the Artist you were looking for in your last question. Please ask again, similar to this example. " +
                            "Alexa, ask The Music Man where Iron Maiden is playing.");
                }
                strTheArtist = myArtistSlot.getValue().toLowerCase();
                log.warn("Artist slot input recieved: " + strTheArtist);
                //This is if the user is spelling out a name, or for some names like
                strTheArtist = strTheArtist.replace(". ", "");
                strTheArtist = strTheArtist.replace(".", "");
                strTheArtist = strTheArtist.replace(" artist ", " ");
                events = getArtistVenueDates(strTheArtist, "Artist");
            }
            catch (Exception ex) {
                return returnFailSpeech( "I couldn't understand the artist or location you were looking for in your last question. Please ask again, similar to one of these examples. " +
                        "Alexa, ask The Music Man who's coming to Staples Center, or if you're looking for an artist, try, Alexa, ask The Music Man where Iron Maiden is playing.");
            }
        } else {
            try {
                myVenueSlot = slots.get(VENUE_SLOT);
                if (null == myVenueSlot) {
                    return returnFailSpeech("I couldn't understand the location you were looking for in your last question. Please ask again, similar to this example. " +
                            "Alexa, ask The Music Man who's coming to Staples Center.");
                }
                strTheVenue = myVenueSlot.getValue();
                log.warn("Venue slot input recieved: " + strTheVenue);
                events = getArtistVenueDates(strTheVenue, "Venue");
            }
            catch (Exception ex) {
                return returnFailSpeech( "I couldn't understand the artist or location you were looking for in your last question. Please ask again, similar to one of these examples. " +
                        "Alexa, ask The Music Man who's coming to Staples Center, or if you're looking for an artist, try, Alexa, ask The Music Man where Iron Maiden is playing.");
            }
        }

        //If weve reached this point, there WAS a value provided in the intent slot
        currentIndex = 0;

        //There may not be any events, or the Songkick service may not recognize the value
        if (null == events || events.isEmpty()) {
            if  ("MusicManArtistIntent".equals(intentName)) {
                if (strTheArtist.endsWith("s")) {
                    speechOutput = String.format("I couldn't find any events where %s are playing", strTheArtist);
                    cardTitle = "Here is where " + strTheArtist + " are playing";
                    cardPrefixContent = "Upcoming dates for " + strTheArtist + ":\n";
                } else {
                    speechOutput = String.format("I couldn't find any events where %s is playing", strTheArtist);
                    cardTitle = "Here is where " + strTheArtist + " is playing";
                    cardPrefixContent = "Upcoming dates for " + strTheArtist + ":\n";
                }
            } else {
                speechOutput = String.format("I couldnt find any upcoming events at %s", strTheVenue);
                cardTitle = "Upcoming shows at " + strTheVenue;
                cardPrefixContent = "Upcoming shows at " + strTheVenue  + ":\n";
            }

            cardPrefixContent = cardPrefixContent + "\r\nEvent data provided by Songkick - visit www.songkick.com";

            SpeechletResponse response;
            response = newTellResponse("<speak>" + speechOutput + "</speak>", true);

            StandardCard card2 = new StandardCard();
            card2.setTitle(cardTitle);
            card2.setText(cardPrefixContent);
            card2.setImage(myImage);

            response.setCard(card2);
            return response;
        }
        else {
            //Establishing speech and card data here, as the 'values' that were provided by the user
            //may have been automatically corrected by the program when it was searching for data.
            if  ("MusicManArtistIntent".equals(intentName))  {
                if (strTheArtist.endsWith("s")) {
                    speechPrefixContent= "<p>Here is where " + strTheArtist + " " + "are playing</p> ";
                    cardTitle = "Here is where " + strTheArtist + " are playing";
                    cardPrefixContent = "Upcoming dates for " + strTheArtist + ":\n";
                } else {
                    speechPrefixContent= "<p>Here is where " + strTheArtist + " " + "is playing</p> ";
                    cardTitle = "Here is where " + strTheArtist + " is playing";
                    cardPrefixContent = "Upcoming dates for " + strTheArtist + ":\n";
                }

             } else {
                speechPrefixContent = "<p>Upcoming shows at " + strTheVenue + " " + "</p> ";
                cardTitle = "Upcoming shows at " + strTheVenue;
                cardPrefixContent = "Upcoming shows at " + strTheVenue  + ":\n";
            }

            StringBuilder speechOutputBuilder = new StringBuilder();
            speechOutputBuilder.append(speechPrefixContent);
            StringBuilder cardOutputBuilder = new StringBuilder();
            cardOutputBuilder.append(cardPrefixContent);

            for (int i = 0; i < PAGINATION_SIZE; i++) {
                try {
                    if (null != events.get(i)) {
                        speechOutputBuilder.append("<s>");
                        speechOutputBuilder.append(events.get(i));
                        speechOutputBuilder.append("</s> ");
                        cardOutputBuilder.append(events.get(i));
                        cardOutputBuilder.append("\n");
                        currentIndex++;
                    }
                } catch (Exception ex) {
                    //May have been less than 3 entries, so an out of bounds may have been thrown.
                    //lets just get out
                }
            }

            if (events.size() > currentIndex) {
                speechOutputBuilder.append(" Would you like to hear more?");
                cardOutputBuilder.append(" Would you like to hear more?");
                repromptText = " Would you like to hear more?";
                // If there are more than 3 events, set the count to the currentIndex and add the events
                // to the session attributes
                session.setAttribute(SESSION_INDEX, currentIndex);
                session.setAttribute(SESSION_TEXT, events);
                session.setAttribute(SESSION_ARTISTVENUE, strArtistVenue);

            } else {
                repromptText = "";
            }

            cardOutputBuilder.append("\r\nEvent data provided by Songkick - visit www.songkick.com");

            speechOutput = speechOutputBuilder.toString();
            //final cleanups to remove troublesome SSML chars
            speechOutput = speechOutput.replace("(", "");
            speechOutput = speechOutput.replace(")", "");
            speechOutput = speechOutput.replace("&", " and ");
            log.warn("Providing response to user: " + speechOutput);

            StandardCard card2 = new StandardCard();
            card2.setTitle(cardTitle);
            card2.setText(cardOutputBuilder.toString());
            card2.setImage(myImage);

            SpeechletResponse response;
            //From above, repromptText was set if more data was available, and we need to ask the user
            //if they want to hear more.. If so, use the newAskResponse, if no pagination, then use newTellResponse
            if (repromptText != "") {
                response  = newAskResponse("<speak>" + speechOutput + "</speak>", true, repromptText, false);
            } else {
                response = newTellResponse("<speak>" + speechOutput + "</speak>", true);
                response.setShouldEndSession(true);
             }
            response.setCard(card2);
            return response;
        }
    }

    /**
     * Creates a {@code SpeechletResponse} for the Welcome intent.
     *
     * @return SpeechletResponse spoken and visual response for the given intent
     */
    protected SpeechletResponse getWelcomeResponse() {
        String speechText = "Hello!, The Music Man is here to tell you where your favorite artist is playing, or who's coming to a " +
                "particular venue. Try asking a question similar to one of these:  " +
                " Who is coming to Staples Center, or,  Where is Iron Maiden playing?";

        String repromptText = "Try asking a question similar to one of these:  " +
                " Who's coming to Staples Center, or,  Where is Iron Maiden playing?";

        StandardCard card2 = new StandardCard();
        card2.setTitle("MusicMan - Welcome!");
        card2.setText(repromptText);
        card2.setImage(myImage);

        // Create the plain text output.
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        SpeechletResponse response = newAskResponse(speechText, false, repromptText, false);
        response.setCard(card2);
        return response;

    }

    /**
     * Creates a {@code SpeechletResponse} for the Help intent.
     *
     * @return SpeechletResponse spoken and visual response for the given intent
     */
    protected SpeechletResponse getHelpResponse() {
        String speechText = "Hello!, You can ask The Music Man where your favorite artists are playing, or who's coming to your favorite venue. " +
                "Take a look at your Alexa companion app for a few examples. Try asking a question similar to one of these,  " +
                 " Who is coming to Staples Center, or, Where is Iron Maiden playing?";


        String repromptText = "Try asking a question similar to one of these:  " +
                " Who is coming to Staples Center, or,  Where is Iron Maiden playing?";

        StandardCard card2 = new StandardCard();
        card2.setTitle("MusicMan - Help");
        card2.setText(speechText);
        card2.setImage(myImage);

        SpeechletResponse response = newAskResponse(speechText, false, repromptText, false);
        response.setCard(card2);
        return response;
    }

    /**
     * queryMusicManParmTable is used to interface with a dynamoDB table for the purposes of locating a corrected text value of something
     * Alexa may have captured from the user incorrectly. Some input as captured by Alexa is not valid for sending to Songkick, so
     *            it needs to be converted to an accurate parm. e.g. Alexa may interpret the users request for artist 'Government Mule', but
     *            Songkick recognizes the artist as Gov't Mule. This table provides the swap value.

     *
     * @param strArtistValue
     *            String object containing the incoming text (as captured by Alexa).
     *
     * @return SpeechletResponse object with voice/card response to return to the user
     */
    protected String queryMusicManParmTable(String strArtistValue) {

        String strTextValue = strArtistValue.toLowerCase();

         try {

            log.warn("Creating AmazonDynamoDBClient");
            AmazonDynamoDBClient client = new AmazonDynamoDBClient()
                    .withRegion(Regions.US_EAST_1);

            DynamoDB dynamoDB = new DynamoDB(client);

            Table table = dynamoDB.getTable("MusicManParmTable");
            log.warn("Finished creating AmazonDynamoDBClient, and connecting table to dynamoDB MusicManParmTable");


            Item item = table.getItem("SongKickInvalidParm", strTextValue);
            if (null!=item) {
                String theReturnedValue = item.getString("SongKickValidParm");
                log.warn("Found an item to swap in the MusicManParmTable: " + theReturnedValue);
                strTextValue =  theReturnedValue;
             } else {
                log.warn("Did not find a value to swap in the MusicManParmTable for " + strTextValue);
            }

            dynamoDB.shutdown();
            client.shutdown();

        }
        catch (Exception e) {

            if (e.getMessage().contains("Connection pool shut down")) {

                log.warn("Connection pool closure");
            }
            //Item not found, or an error - in either case, just return what the user had provided
        }

        return strTextValue;

    }

    /**
     * Creates a {@code ArrayList<String>} to provide the Intent service handler the events they need to formulate tor saying.
     *
     * @param  strTextValue contains the string text object to be processed
     * @param  requestType contains a value of either 'Venue' or 'Artist' to allow the function to know which URLs to call at SongKick
     * @return ArrayList contains the String array of respective events for either Artists OR Venue Calendars
     */
    protected ArrayList<String> getArtistVenueDates(String strTextValue, String requestType) {

        //Try to do some preliminary cleanup of things that can throw Alexa or SongKick.
        //hyphonation periods, etc.
        switch (requestType) {

            case "Venue":
                //Some venue names can be misunderstood - query dynamodb MusicManParmTable that houses
                //a list of common failed items - e.g. madison square gardeners should be madison square gardens
                strTextValue = strTextValue.toLowerCase().replace("u. s.", "US");   //e.g. U. S. Bank Arena should be US Bank Arena
                strTextValue = strTextValue.toLowerCase().replace("a. t. and t", "AT&T");
                strTextValue = strTextValue.toLowerCase().replace(" marina", " Arena");
                strTextValue = strTextValue.toLowerCase().replace(" farina", " Arena");
                strTextValue = strTextValue.toLowerCase().replace("amplitheater", "Amphitheater");
                //Update the value used in the speech with any corrections that may have been made
                log.info("Begin query the MusicManParmTable for the ArtistVenue Value");
                strTextValue = queryMusicManParmTable(strTextValue);
                log.info("End query the MusicManParmTable");

                strTheVenue = strTextValue;
                strArtistVenue = strTheVenue;
                log.info("Processing Venue request for: " + strTheVenue);

                break;
            case "Artist":
                //Artist names may be frequently misunderstood - query dynamodb MusicManParmTable that houses
                //a list of common failed items - e.g. government mule should be Gov't Mule when querying Songkick..
                log.info("Begin query the MusicManParmTable for the ArtistVenue Value");
                strTextValue = queryMusicManParmTable(strTextValue);
                log.info("End query the MusicManParmTable");
                //Update the strTheArtist variable with any updates (in case there was a correction available
                //in the DynamoDB table
                strTheArtist = strTextValue;
                strArtistVenue = strTheArtist;

                log.info("Processing Artist request for: " + strTheArtist);
              //  java.util.Date date = new java.util.Date();
              //  log.info("Returned from queryArtistFix with: " +  strTextValue + " at: " + date);
                break;
        }

        String strURLEncodedParm = "";
        try {
            strURLEncodedParm = URLEncoder.encode(strTextValue, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            log.error("Error in URLEncoding the parm: " + strTextValue);
            return null;
        }

        String strURLRequest = "";
        switch (requestType) {
            case "Venue":
                strURLRequest = "http://api.songkick.com/api/3.0/search/venues.json?query=" + strURLEncodedParm + "&apikey=" + strSongKickAPIID;
                //Comment out after testing
                 break;
            case "Artist":
                strURLRequest= "http://api.songkick.com/api/3.0/search/artists.json?query=" + strURLEncodedParm + "&apikey=" + strSongKickAPIID;
                //Comment out after testing
                break;
        }

        String strVenueID = "";
        String strArtistID = "";

        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {

            String strRequestString = strURLRequest;

            HttpGet httpget = new HttpGet(strRequestString);

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }
            };

            log.info("Executing httpclient request");
            String responseBody = httpclient.execute(httpget, responseHandler);
            log.info("httpclient request returned");

            //Jackson JSON
            log.info("Creating JSONFactory and JSONParser, performing readTree");
            ObjectMapper mapper=new ObjectMapper();
            JsonFactory factory=mapper.getFactory();
            JsonParser jp=factory.createParser(responseBody);
            JsonNode input=mapper.readTree(jp);
            log.info("Completed JSONFactory and JSONParser creation and readTree");

            if (strRequestString.contains("venues.json")) {
                //Parse the response as a Venue call, get the Venue's ID
                try {
                    JsonNode myVenueNode =  input.get("resultsPage").get("results").get("venue").get(0);
                    strVenueID = myVenueNode.get("id").asText();

                } catch (Exception ex) {
                    log.warn("Did not find venue id for: " + strURLEncodedParm);
                    return null;
                }
                //Now use the venue ID to get the calendar for the venue
                String strVenueCalendrRequest = "http://api.songkick.com/api/3.0/venues/" + strVenueID + "/calendar.json?apikey=" + strSongKickAPIID;
                httpget = new HttpGet(strVenueCalendrRequest);
                responseBody = httpclient.execute(httpget, responseHandler);

                 jp=factory.createParser(responseBody);
                input=mapper.readTree(jp);

                try {
                  //  int intTotalEntries = JsonPath.read(document, "$.resultsPage.totalEntries");
                    int intTotalEntries = input.get("resultsPage").findValue("totalEntries").asInt();
                    if (intTotalEntries > 0) {
                        String theEvent = "";
                        ArrayList<String> strVenueCalendarEntries = new ArrayList<String>();

                        JsonNode strVenueCalendarValues1 = input.get("resultsPage").get("results").findValue("event");
                        for(Iterator<JsonNode> i = strVenueCalendarValues1.iterator(); i.hasNext(); ) {
                            JsonNode item = i.next();
                            theEvent = item.get("displayName").asText();

                            //Remove the venue name included in the calendar of responses
                            int loc1 = theEvent.indexOf(" at ");
                            int loc2 = theEvent.indexOf(" (");

                            if (loc1 > 1  && loc2 > loc1) {
                                //found the venue included in the response... at <some location> (
                                //e.g. Buckethead at Cain's Ballroom (June 13, 2016)
                                //Remove the ' at Cain's Ballroom ' part.
                                theEvent = theEvent.substring(0,loc1) + ", on " + theEvent.substring(loc2);
                            }
                            //last edit check
                            strVenueCalendarEntries.add(theEvent);
                        }
                        return strVenueCalendarEntries;

                    } else {
                        //No calendar entries for this Artist
                        return null;
                      }
                } catch (Exception ex) {
                    return null;
                }
            } // end If Venue
            else {
                //Parse the response as a Artist call, get the Artst's ID

                try {
                    JsonNode myArtistNode =  input.get("resultsPage").get("results").get("artist").get(0);
                    strArtistID = myArtistNode.get("id").asText();
                } catch (Exception ex) {
                    log.warn("Did not find artist id for: " + strURLEncodedParm);
                    return null;
                }

                //Now use the Artist ID to get their calendar of upcoming events
                String strVenueCalendrRequest = "http://api.songkick.com/api/3.0/artists/" + strArtistID + "/calendar.json?apikey=" + strSongKickAPIID;
                httpget = new HttpGet(strVenueCalendrRequest);
                responseBody = httpclient.execute(httpget, responseHandler);

                jp=factory.createParser(responseBody);
                input=mapper.readTree(jp);

                try {
                    int intTotalEntries = input.get("resultsPage").findValue("totalEntries").asInt();

                    if (intTotalEntries > 0) {
                        ArrayList<String> strArrayArtistsCalendarEntries = new ArrayList<String>();
                        String theEvent = "";

                        JsonNode strArtistCalendarValues1 = input.get("resultsPage").get("results").findValue("event");
                        for(Iterator<JsonNode> i = strArtistCalendarValues1.iterator(); i.hasNext(); ) {
                            JsonNode item = i.next();
                            JsonNode cityItem = item.get("location");
                            theEvent = item.get("displayName").asText() + " in " + cityItem.get("city").asText().replace(", US", "");

                            int locOfAt = theEvent.lastIndexOf(" at ");
                            String tempString = theEvent.replace(" (", ", on (");
                            tempString = tempString.replace("on (CANCELLED)", " is Cancelled.");

                            //log.info("Artist Event: " + tempString);

                            if (locOfAt >= 0) {
                                strArrayArtistsCalendarEntries.add(tempString.substring(locOfAt));
                            } else {
                                strArrayArtistsCalendarEntries.add(tempString);
                            }
                        }
                        log.info("Returning Array of Artist Calendar events, size: " + strArrayArtistsCalendarEntries.size());
                        return strArrayArtistsCalendarEntries;

                    } else {
                        //No calendar entries for this Artist
                        log.info("Returning null for Artist Calendar events");

                        return null;
                    }
                } catch (Exception ex) {
                    return null;
                }
            } //end If Artist

        } catch (IOException ex) {

        } finally {
            try {
                httpclient.close();
            } catch (IOException ex) {
            }
        }
        return null;
    }

    /**
     * Wrapper for creating the Tell response. This newTellResponse takes care of providing the right Speechlet response
     * using either SSML, or plain text based on the value in {@code isOutputSsml}
     *
     * @param stringOutput
     *            the output to be spoken
     * @param isOutputSsml
     *            whether the output text is of type SSML
     * @return SpeechletResponse the speechlet response
     */
    protected SpeechletResponse newTellResponse(String stringOutput, boolean isOutputSsml) {
        OutputSpeech outputSpeech;

        log.info("Processing newTellResponse");
        if (isOutputSsml) {
            outputSpeech = new SsmlOutputSpeech();
            ((SsmlOutputSpeech) outputSpeech).setSsml(stringOutput);
        } else {
            outputSpeech = new PlainTextOutputSpeech();
            ((PlainTextOutputSpeech) outputSpeech).setText(stringOutput);
        }

        return SpeechletResponse.newTellResponse(outputSpeech);
    }

    /**
     * Wrapper for creating the Ask response. This newAskResponse takes care of providing the right Speechlet response
     * using either SSML, or plain text based on the value(s) in {@code isOutputSsml} and {@code isRepromptSsml}
     *
     * @param stringOutput
     *            the output to be spoken
     * @param isOutputSsml
     *            whether the output text is of type SSML
     * @param repromptText
     *            the reprompt for if the user doesn't reply or is misunderstood.
     * @param isRepromptSsml
     *            whether the reprompt text is of type SSML
     * @return SpeechletResponse the speechlet response
     */
    protected SpeechletResponse newAskResponse(String stringOutput, boolean isOutputSsml,
                                             String repromptText, boolean isRepromptSsml) {
        OutputSpeech outputSpeech, repromptOutputSpeech;

        log.info("Processing newAskResponse");

        if (isOutputSsml) {
            outputSpeech = new SsmlOutputSpeech();
            ((SsmlOutputSpeech) outputSpeech).setSsml(stringOutput);
        } else {
            outputSpeech = new PlainTextOutputSpeech();
            ((PlainTextOutputSpeech) outputSpeech).setText(stringOutput);
        }

        if (isRepromptSsml) {
            repromptOutputSpeech = new SsmlOutputSpeech();
            ((SsmlOutputSpeech) repromptOutputSpeech).setSsml(repromptText);
        } else {
            repromptOutputSpeech = new PlainTextOutputSpeech();
            ((PlainTextOutputSpeech) repromptOutputSpeech).setText(repromptText);
        }
        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(repromptOutputSpeech);
        return SpeechletResponse.newAskResponse(outputSpeech, reprompt);
    }
}
