MusicManArtistIntent for artist {Rihanna|artist}
MusicManArtistIntent for artist {Iron Maiden|artist}
MusicManArtistIntent for artist {The Rolling Stones|artist}
MusicManArtistIntent for artist {Five Finger Death Punch|artist}
MusicManArtistIntent for artist {Tom Petty and the Heartbreakers|artist}
MusicManArtistIntent for artist {Tom Petty and the Heart breakers|artist}

MusicManArtistIntent where {Rihanna|artist} is playing
MusicManArtistIntent where {Iron Maiden|artist} is playing
MusicManArtistIntent where {The Rolling Stones|artist} is playing
MusicManArtistIntent where {Five Finger Death Punch|artist} is playing
MusicManArtistIntent where {Tom Petty and the Heartbreakers|artist} is playing
MusicManArtistIntent where {Tom Petty and the Heart breakers|artist} is playing

MusicManArtistIntent where {Rihanna|artist} are playing
MusicManArtistIntent where {Iron Maiden|artist} are playing
MusicManArtistIntent where {The Rolling Stones|artist} are playing
MusicManArtistIntent where {Five Finger Death Punch|artist} are playing
MusicManArtistIntent where {Tom Petty and the Heartbreakers|artist} are playing
MusicManArtistIntent where {Tom Petty and the Heart breakers|artist} are playing

MusicManArtistIntent where are {Rihanna|artist}
MusicManArtistIntent where are {Iron Maiden|artist}
MusicManArtistIntent where are {The Rolling Stones|artist}
MusicManArtistIntent where are {Five Finger Death Punch|artist}
MusicManArtistIntent where are {Tom Petty and the Heartbreakers|artist}
MusicManArtistIntent where are {Tom Petty and the Heart breakers|artist}

MusicManArtistIntent where are {Rihanna|artist} playing
MusicManArtistIntent where are {Iron Maiden|artist} playing
MusicManArtistIntent where are {The Rolling Stones|artist} playing
MusicManArtistIntent where are {Five Finger Death Punch|artist} playing
MusicManArtistIntent where are {Tom Petty and the Heartbreakers|artist} playing
MusicManArtistIntent where are {Tom Petty and the Heart breakers|artist} playing

MusicManArtistIntent where is {Rihanna|artist}
MusicManArtistIntent where is {Iron Maiden|artist}
MusicManArtistIntent where is {The Rolling Stones|artist}
MusicManArtistIntent where is {Five Finger Death Punch|artist}
MusicManArtistIntent where is {Tom Petty and the Heartbreakers|artist}
MusicManArtistIntent where is {Tom Petty and the Heart breakers|artist}

MusicManArtistIntent where is {Rihanna|artist} playing
MusicManArtistIntent where is {Iron Maiden|artist} playing
MusicManArtistIntent where is {The Rolling Stones|artist} playing
MusicManArtistIntent where is {Five Finger Death Punch|artist} playing
MusicManArtistIntent where is {Tom Petty and the Heartbreakers|artist} playing
MusicManArtistIntent where is {Tom Petty and the Heart breakers|artist} playing

MusicManArtistIntent when are {Rihanna|artist}
MusicManArtistIntent when are {Iron Maiden|artist}
MusicManArtistIntent when are {The Rolling Stones|artist}
MusicManArtistIntent when are {Five Finger Death Punch|artist}
MusicManArtistIntent when are {Tom Petty and the Heartbreakers|artist}
MusicManArtistIntent when are {Tom Petty and the Heart breakers|artist}

MusicManArtistIntent when are {Rihanna|artist} playing
MusicManArtistIntent when are {Iron Maiden|artist} playing
MusicManArtistIntent when are {The Rolling Stones|artist} playing
MusicManArtistIntent when are {Five Finger Death Punch|artist} playing
MusicManArtistIntent when are {Tom Petty and the Heartbreakers|artist} playing
MusicManArtistIntent when are {Tom Petty and the Heart breakers|artist} playing

MusicManArtistIntent when is {Rihanna|artist}
MusicManArtistIntent when is {Iron Maiden|artist}
MusicManArtistIntent when is {The Rolling Stones|artist}
MusicManArtistIntent when is {Five Finger Death Punch|artist}
MusicManArtistIntent when is {Tom Petty and the Heartbreakers|artist}
MusicManArtistIntent when is {Tom Petty and the Heart breakers|artist}

MusicManArtistIntent when is {Rihanna|artist} playing
MusicManArtistIntent when is {Iron Maiden|artist} playing
MusicManArtistIntent when is {The Rolling Stones|artist} playing
MusicManArtistIntent when is {Five Finger Death Punch|artist} playing
MusicManArtistIntent when is {Tom Petty and the Heartbreakers|artist} playing
MusicManArtistIntent when is {Tom Petty and the Heart breakers|artist} playing

MusicManArtistIntent when {Rihanna|artist} is playing
MusicManArtistIntent when {Iron Maiden|artist} is playing
MusicManArtistIntent when {The Rolling Stones|artist} is playing
MusicManArtistIntent when {Five Finger Death Punch|artist} is playing
MusicManArtistIntent when {Tom Petty and the Heartbreakers|artist} is playing
MusicManArtistIntent when {Tom Petty and the Heart breakers|artist} is playing

MusicManArtistIntent when {Rihanna|artist} are playing
MusicManArtistIntent when {Iron Maiden|artist} are playing
MusicManArtistIntent when {The Rolling Stones|artist} are playing
MusicManArtistIntent when {Five Finger Death Punch|artist} are playing
MusicManArtistIntent when {Tom Petty and the Heartbreakers|artist} are playing
MusicManArtistIntent when {Tom Petty and the Heart breakers|artist} are playing

MusicManVenueIntent for location {Mohawk|venue}
MusicManVenueIntent for location {Comerica Theater|venue}
MusicManVenueIntent for location {American Airlines Arena|venue}
MusicManVenueIntent for location {Talking Stick Resort Arena|venue}
MusicManVenueIntent for location {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent for shows at {Mohawk|venue}
MusicManVenueIntent for shows at {Comerica Theater|venue}
MusicManVenueIntent for shows at {American Airlines Arena|venue}
MusicManVenueIntent for shows at {Talking Stick Resort Arena|venue}
MusicManVenueIntent for shows at {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent for shows coming to {Mohawk|venue}
MusicManVenueIntent for shows coming to {Comerica Theater|venue}
MusicManVenueIntent for shows coming to {American Airlines Arena|venue}
MusicManVenueIntent for shows coming to {Talking Stick Resort Arena|venue}
MusicManVenueIntent for shows coming to {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent for events at {Mohawk|venue}
MusicManVenueIntent for events at {Comerica Theater|venue}
MusicManVenueIntent for events at {American Airlines Arena|venue}
MusicManVenueIntent for events at {Talking Stick Resort Arena|venue}
MusicManVenueIntent for events at {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent for events coming to {Mohawk|venue}
MusicManVenueIntent for events coming to {Comerica Theater|venue}
MusicManVenueIntent for events coming to {American Airlines Arena|venue}
MusicManVenueIntent for events coming to {Talking Stick Resort Arena|venue}
MusicManVenueIntent for events coming to {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent who's at {Mohawk|venue}
MusicManVenueIntent who's at {Comerica Theater|venue}
MusicManVenueIntent who's at {American Airlines Arena|venue}
MusicManVenueIntent who's at {Talking Stick Resort Arena|venue}
MusicManVenueIntent who's at {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent who's playing {Mohawk|venue}
MusicManVenueIntent who's playing {Comerica Theater|venue}
MusicManVenueIntent who's playing {American Airlines Arena|venue}
MusicManVenueIntent who's playing {Talking Stick Resort Arena|venue}
MusicManVenueIntent who's playing {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent who's playing at {Mohawk|venue}
MusicManVenueIntent who's playing at {Comerica Theater|venue}
MusicManVenueIntent who's playing at {American Airlines Arena|venue}
MusicManVenueIntent who's playing at {Talking Stick Resort Arena|venue}
MusicManVenueIntent who's playing at {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent who's coming to {Mohawk|venue}
MusicManVenueIntent who's coming to {Comerica Theater|venue}
MusicManVenueIntent who's coming to {American Airlines Arena|venue}
MusicManVenueIntent who's coming to {Talking Stick Resort Arena|venue}
MusicManVenueIntent who's coming to {Bank of New Hampshire Pavilion|venue}


MusicManVenueIntent who is playing at {Mohawk|venue}
MusicManVenueIntent who is playing at {Comerica Theater|venue}
MusicManVenueIntent who is playing at {American Airlines Arena|venue}
MusicManVenueIntent who is playing at {Talking Stick Resort Arena|venue}
MusicManVenueIntent who is playing at {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent who is coming to {Mohawk|venue}
MusicManVenueIntent who is coming to {Comerica Theater|venue}
MusicManVenueIntent who is coming to {American Airlines Arena|venue}
MusicManVenueIntent who is coming to {Talking Stick Resort Arena|venue}
MusicManVenueIntent who is coming to {Bank of New Hampshire Pavilion|venue}

MusicManArtistIntent and where {Rihanna|artist} is playing
MusicManArtistIntent and where {Iron Maiden|artist} is playing
MusicManArtistIntent and where {The Rolling Stones|artist} is playing
MusicManArtistIntent and where {Five Finger Death Punch|artist} is playing
MusicManArtistIntent and where {Tom Petty and the Heartbreakers|artist} is playing
MusicManArtistIntent and where {Tom Petty and the Heart breakers|artist} is playing

MusicManArtistIntent and where is {Rihanna|artist}
MusicManArtistIntent and where is {Iron Maiden|artist}
MusicManArtistIntent and where is {The Rolling Stones|artist}
MusicManArtistIntent and where is {Five Finger Death Punch|artist}
MusicManArtistIntent and where is {Tom Petty and the Heartbreakers|artist}
MusicManArtistIntent and where is {Tom Petty and the Heart breakers|artist}

MusicManArtistIntent and where is {Rihanna|artist} playing
MusicManArtistIntent and where is {Iron Maiden|artist} playing
MusicManArtistIntent and where is {The Rolling Stones|artist} playing
MusicManArtistIntent and where is {Five Finger Death Punch|artist} playing
MusicManArtistIntent and where is {Tom Petty and the Heartbreakers|artist} playing
MusicManArtistIntent and where is {Tom Petty and the Heart breakers|artist} playing

MusicManArtistIntent and when is {Rihanna|artist}
MusicManArtistIntent and when is {Iron Maiden|artist}
MusicManArtistIntent and when is {The Rolling Stones|artist}
MusicManArtistIntent and when is {Five Finger Death Punch|artist}
MusicManArtistIntent and when is {Tom Petty and the Heartbreakers|artist}
MusicManArtistIntent and when is {Tom Petty and the Heart breakers|artist}

MusicManArtistIntent and when is {Rihanna|artist} playing
MusicManArtistIntent and when is {Iron Maiden|artist} playing
MusicManArtistIntent and when is {The Rolling Stones|artist} playing
MusicManArtistIntent and when is {Five Finger Death Punch|artist} playing
MusicManArtistIntent and when is {Tom Petty and the Heartbreakers|artist} playing
MusicManArtistIntent and when is {Tom Petty and the Heart breakers|artist} playing

MusicManVenueIntent and who's at {Mohawk|venue}
MusicManVenueIntent and who's at {Comerica Theater|venue}
MusicManVenueIntent and who's at {American Airlines Arena|venue}
MusicManVenueIntent and who's at {Talking Stick Resort Arena|venue}
MusicManVenueIntent and who's at {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent and who's playing {Mohawk|venue}
MusicManVenueIntent and who's playing {Comerica Theater|venue}
MusicManVenueIntent and who's playing {American Airlines Arena|venue}
MusicManVenueIntent and who's playing {Talking Stick Resort Arena|venue}
MusicManVenueIntent and who's playing {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent and who's playing at {Mohawk|venue}
MusicManVenueIntent and who's playing at {Comerica Theater|venue}
MusicManVenueIntent and who's playing at {American Airlines Arena|venue}
MusicManVenueIntent and who's playing at {Talking Stick Resort Arena|venue}
MusicManVenueIntent and who's playing at {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent and who's coming to {Mohawk|venue}
MusicManVenueIntent and who's coming to {Comerica Theater|venue}
MusicManVenueIntent and who's coming to {American Airlines Arena|venue}
MusicManVenueIntent and who's coming to {Talking Stick Resort Arena|venue}
MusicManVenueIntent and who's coming to {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent and who is playing at {Mohawk|venue}
MusicManVenueIntent and who is playing at {Comerica Theater|venue}
MusicManVenueIntent and who is playing at {American Airlines Arena|venue}
MusicManVenueIntent and who is playing at {Talking Stick Resort Arena|venue}
MusicManVenueIntent and who is playing at {Bank of New Hampshire Pavilion|venue}

MusicManVenueIntent and who is coming to {Mohawk|venue}
MusicManVenueIntent and who is coming to {Comerica Theater|venue}
MusicManVenueIntent and who is coming to {American Airlines Arena|venue}
MusicManVenueIntent and who is coming to {Talking Stick Resort Arena|venue}
MusicManVenueIntent and who is coming to {Bank of New Hampshire Pavilion|venue}

GetNextEventIntent yes
GetNextEventIntent yup
GetNextEventIntent sure
GetNextEventIntent yes please
GetNextEventIntent please
GetNextEventIntent ok
GetNextEventIntent absolutely
GetNextEventIntent fine
GetNextEventIntent if you must

StopEvents no
StopEvents no thanks
StopEvents no thank you
StopEvents nope
StopEvents stop
StopEvents quit
StopEvents end
StopEvents leave
StopEvents bye
StopEvents good bye
StopEvents cancel
StopEvents enough
StopEvents please stop

IncompleteIntents where
IncompleteIntents where is
IncompleteIntents where are
IncompleteIntents where's'
IncompleteIntents who
IncompleteIntents who's
IncompleteIntents what
IncompleteIntents when
IncompleteIntents when is
IncompleteIntents when are
IncompleteIntents ask

HelpEvents for help
HelpEvents to help me
HelpEvents to please help