# AWS Alexa Music Man skill

##Alexa/MusicMan Skills Kit Documentation
This project is meant to allow an Amazon Alexa consumer, ability to ask for information related to where an Artist (band, etc) may be playing, or what artists
may be coming to a Venue.
It does so by providing a set of Alexa skill intents, for an Artist or Venue query. The code behind this will operate in Amazon Lambda, and upon recieval of
an intent, it will attempt to determine what the consumer is asking for and make an http get to the SONGKICK api site.

See http://www.songkick.com/
http://www.songkick.com/developer

![Image of Songkick ](http://static.tumblr.com/yfms2o4/HKnl73h3y/logo_for_tumblr_fullname.png) 

## Contents
 - **MusicMan** - This app is the core Speechlet app that runs in Amazon Lambda, with a speech stream handler that invokes the main class that makes the Songkick api calls and then constructs the results into
 a (Speech Markup language) that is fed back to Alexa for audibly reciting the results.

### Required for the projects to run
A **config.properties** is needed to house the Songkick API credential - so as to not
 be hard coded inside the app.  This file should be placed inside the resources dir

config.properties should have the following entry:
```
apikey="your Songkick API id"
```

A **log4j.properties** file should also be present in the **src/main/java/resources** directory. For simple Cloudwatch
console logging, use the following lines. **WARN** level is used simply to provide a means to have
two levels of logging for the more important and less important log lines for MusicMan. That way,
debug is only if you need to see the real low level details of the AWS, Apache http, etc. calls.

```
# Root logger option
log4j.rootLogger=WARN, stdout

# Redirect log messages to console
log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.Target=System.out
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n
```